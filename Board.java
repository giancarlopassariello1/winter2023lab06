public class Board {
	
	private Die p1;
    private Die p2;
	private boolean[] tiles;
	
	public Board() {
		this.p1 = new Die();
		this.p2 = new Die();
		this.tiles = new boolean[12];
	}
	
	public String toString() {
		String t = "";
		int x = 1;
		for (int i = 0; i < this.tiles.length; i++) {
			if (tiles[i] == false) {
				t = t+x+" ";
			} else {
				t = t+"X"+" ";
			}
			x++; 
		}
			return t;
		}
	
	public boolean playATurn() {
		this.p1.roll();
		this.p2.roll();
		System.out.println(this.p1.getFaceValue());
		System.out.println(this.p2.getFaceValue());
		int sumOfDice = this.p1.getFaceValue() + this.p2.getFaceValue();
		
		if (this.tiles[sumOfDice - 1] == false) {
			this.tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile: "+sumOfDice);
			return false;
		} else if (this.tiles[this.p1.getFaceValue()] == false) {
			this.tiles[this.p1.getFaceValue()] = true;
			System.out.println("Closing tile with the same value as die one: "+this.p1.getFaceValue());
			return false;
		} else if (this.tiles[this.p2.getFaceValue()] == false) {
			this.tiles[this.p2.getFaceValue()] = true;
			System.out.println("Closing tile with the same value as die two: "+this.p2.getFaceValue());
			return false;
		} else {
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
	}
}