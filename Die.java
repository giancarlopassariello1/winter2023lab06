import java.util.Random;
public class Die {
	
	private int faceValue;
	private Random rand;
	

	public Die() {
		this.faceValue = 1;
		this.rand = new Random();
	}
	
	public int getFaceValue() {
		return this.faceValue;
	}
	
	public void roll() {
		this.faceValue = this.rand.nextInt(6)+1;
	}
	
	public String toString() {
		return "face value: "+this.faceValue;
	}
}