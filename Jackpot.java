public class Jackpot {
	public static void main(String[] args) {
		System.out.println("Welcome to Shut the Box!");
		Board b = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while (gameOver == false) {
			System.out.println(b);
			
			if(b.playATurn() == true) {
				gameOver = true;
			} else {
				numOfTilesClosed++;
			}
		}
		
		if (numOfTilesClosed >= 7) {
			System.out.println("You have reached the jackpot and won !");
		} else {
			System.out.println("You lost :(");
		}
	}
}